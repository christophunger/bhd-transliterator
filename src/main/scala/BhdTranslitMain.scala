/* 
 *  This file is part of Bhd-transliterator.
 *  Copyright (C) 2021  Christoph Unger
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package transliterationMain

import java.io._
import scala.io._
import phonemes.Phonemes._
import interpretation.Interpretation._
import preparation.Preparation._
import mappings.CharacterMap._
import scala.util.matching.Regex
import scala.util.Try

object BhdTranslitMain extends App {
  // Reading the obligatory commandline arguments for input and output
  val input = args(0)
  val output = args(1)
  

  // to make the .jar file work on Windows. From scala.io
  implicit val codec = Codec("UTF-8")

  // Opening the input file
  val BufferedSource = Source.fromFile(input)
  val workingon = BufferedSource.getLines().toList
  BufferedSource.close()

  // Core program
  // Prepare the input as a list of strings
  val splittedInput = workingon.flatMap(_.split(" ").toList)

  // map characters from AS to LS
  val charMapped = splittedInput.map(characterTranslit(_))

  // define the post-mapping interpretation pipeline
  def runOnWordsOnly(in: String): String = in match {
    case x if (isSFM(x)) => x
    case x if (!isSFM(x)) => fullTranslitPipe(in)
  }

  def fullTranslitPipe(in: String): String = {
    // disambiguate 'uu' ('uî' is tricky; postpone)
    val charMappedUu = uuToOne(in)
    inserti(disambv(charMappedUu))
  }

  // Switching removal of neutral Vowel in non-licensed 2nd syllable position on
  // with optional commandline argument "on"
  val removeNVon: Option[String] = Try(args(2)).toOption
  // Using wordlist-based optimisation if removeNVon is "on". 
  val useWordlist: Option[String] = Try(args(3)).toOption

  // Opening the Wordlist for words with 'i' in stem
  val wordlistExists = useWordlist match {
    case Some(wls) => wls
    // if no wordlist ist specified, we throw an exception. 
    case None => throw new Exception("No wordlist specified. ")
    }
  val wordlistSource = Source.fromFile(wordlistExists)
  val stemiList = wordlistSource.getLines().toList
  wordlistSource.close()


  val transliterationForOutput = removeNVon match {
    case Some(a) if a == "on" => useWordlist match {
      case Some(b) => charMapped.map(runOnWordsOnly(_)).map(removeWithWordlist(_,stemiList))
      case None =>  charMapped.map(runOnWordsOnly(_)).map(removeUnlicensedNeutralVowel(_))
    }
    case Some(a) => charMapped.map(runOnWordsOnly(_))
    case None =>  charMapped.map(runOnWordsOnly(_))
  }

  // Deciding whether to remove /i/ in the second syllable or not
  def removeUnlicensedNeutralVowel(w: String): String = w match {
    case a if w.filter(allVowels(_)).length < 4 => w
    case _ => removeVowel(w)
  }

    // Deciding whether to remove /i/ in the second syllable or not
  def removeWithWordlist(w: String, wli: List[String]): String = w match {
    case a if w.filter(allVowels(_)).length < 4 => w
    case a if checkWordList(a, wli) => w
    case _ => removeVowel(w)
  }


  // insert line break before SFM in output
  // SFM: Standard Format Marker https://ubsicap.github.io/usfm/usfm3.0.2/about/index.html
  // Essentially, an SFM is \ followed by one or more non-space characters
  val outSfm = transliterationForOutput
    .map(x => if (isSFM(x)) "\n" concat x; else x)
    .mkString(" ")

  // cleanup: remove glottal stop
  val finalOutput = removeGlottalStop(outSfm)

  // Write the result to output file
  // Windows does not use UTF8 by default,
  // so we need to explicitly tell the writer to write UTF8
  val out = new File(output)
  val writerOne = new BufferedWriter(
    new OutputStreamWriter(
      new FileOutputStream(out), "UTF-8"))
  writerOne.write(finalOutput)
  writerOne.close()
  
}

