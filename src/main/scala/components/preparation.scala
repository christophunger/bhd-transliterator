/* 
 *  This file is part of Bhd-transliterator.
 *  Copyright (C) 2021  Christoph Unger
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package preparation

import phonemes.Phonemes._
import scala.util.matching.Regex

object Preparation {
  // Preparation AND cleanup

  // Interpret sequence 'uu' 
  def uuToOne(in: String): String = {
    val regex = "uu".r
    regex.replaceAllIn(in, "û")
  }

  // Find out if the present char sequence is a SFM
  val sfm = """\\\w+""".r 
  def isSFM(in: String): Boolean = {
    val res = sfm findPrefixOf in
    res match {
      case Some(_) => true
      case None => false
    }
  }

  // Is the present char sequence Punctuation?
  // Is this function used anywhere??
  val punct = """[.,:;?!]*""".r
  def isPunctuation(in: String): Boolean = {
    val res = punct findPrefixOf in
    res match {
      case Some(_) => true
      case None => false
    }
  }

  // Some input text have a mixed representation of numerals,
  // they must first be converted to Eastern Arabic numerals.
  val digitSpecial = Map(
    '0' -> '٠',  
    '1' -> '١',
    '2' -> '٢',
    '3' -> '٣',
    '4' -> '٤',
    '5' -> '٥',
    '6' -> '٦',
    '7' -> '٧',
    '8' -> '٨',
    '9' -> '٩'
  )

  def convNumerals(n: Char): Char = {
    val convertedNumeral = digitSpecial get n
      convertedNumeral match {
        case Some(x) => x
        case None => n
      }
  }


  // Clean up afterwards
  // Remove glottal stop word initially
  def removeGlottalStop(in: String): String = {
    val regex = s"([\\s${punct}()])(\u0294)".r
    regex.replaceAllIn(in, m => m.group(1))
  }

  /* 
   * Checking whether a neutral Vowel is licensed
   * Heuristic: 
   * A neutral vowel is licensed if it occurs.. 
   * 1. in the last syllable
   * 2. in the penultimate syllable
   * 3. in the first syllabe
   * 
   * We check every 'i' vowel whether one of these three conditions hold.
   * If none of them holds, the 'i' vowel should be removed, 
   * i.e. we output a copy of this word with this vowel removed. 
   * 
   * Notice that only words with four and more syllables are relevant in this step. 
   * The number of syllables is counted according to the outcome of the preceding procedure. 
   * I.e., "destihelata" is a five syllable word at this step in the analysis. 
   * This is incorrect, the output should be "desthelata", a four syllable word. 
   * 
   * Moreover, words with /i/ in the stem should not be touched by this function.
   */


  // Checking whether the word in question has /i/ in its stem
  def checkWordList(w: String, l: List[String]): Boolean = {
    val boolList: List[Boolean] = for (li <- l) yield {
      val pat = li.r.unanchored
      w match {
        case pat(_*) => true
        case _ => false
      }
    }
    boolList match {
      case _ if boolList.contains(true) => true
      case _ => false
    }
  }


  // Removing /i/ in the second syllable of words with 4 syllables or more
  def removeVowel(w: String): String = {
    // We turn the word from String into a List of characters
    // which we zip with their Index, so we get a List of character-index pairs.
    val wordAsIndexedPairs = w.toList.zipWithIndex
    // We separate vowels from consonants
    val (vowelsOfWordWrong, consonantsOfWordWrong): (List[(Char, Int)], List[(Char, Int)]) = wordAsIndexedPairs
      .partition{
        case (c, i) if allVowels(c) => true
        case _ => false }

    // For the vowels, we make a list of triples (Char, Int, Int)
    // where the first Int is the index in the Word,
    // and the secon Int is the index in the list of Vowels.
    val wordWrongVowels = vowelsOfWordWrong
      .zipWithIndex
      .map{ case ((a, b), c) => (a, b, c)}

    // Assigning license scores for neutral vowels.
    // The neutral vowel is only licensed in the ultimate and penultimate syllable,
    // as well as in the first syllable of a word.
    // Every syllable has one vowel as nucleus, so the number of vowels equals the number of syllables.
    // Neutral vowels in licensed positions get score 1, otherwise score 0.
    // The scores are represented in the fourth element of a quadruple.
    def assignLicenseScore(voweltuples: List[(Char, Int, Int)]): List[(Char, Int, Int, Int)] = {
      val maxind = voweltuples.length
      def assignValue(tu: (Char, Int, Int)): (Char, Int, Int, Int) = tu match {
        //  case (ch, cix, vix) if ch == 'i' && vix > 0 && vix < (maxind - 2) => (ch, cix, vix, 0)
        case (ch, cix, vix) if ch == 'i' && vix == 1 => (ch, cix, vix, 0) // removing only in second syllable
        case (ch, cix, vix) => (ch, cix, vix, 1)
      }

      for (t <- voweltuples) yield assignValue(t)
    }
    val wordWrongVowelsLicense = assignLicenseScore(wordWrongVowels)

    // We remove unlicensed neutral vowels by applying this filter:
    def getIndexToFilter(chartuple: (Char, Int, Int, Int)): Boolean = chartuple match {
      case (_, ix, _, li) if li == 0 => true
      case _ => false
    }
    val filtered = wordWrongVowelsLicense.filterNot(getIndexToFilter(_))
    val remainigVowelPairs = filtered.map(x => (x._1, x._2))

    // Now we are down to vowel-index pairs, so we can merge in the consonants again.
    val remainingVC = remainigVowelPairs ::: consonantsOfWordWrong
    // Restoring the original order according to indeces
    val resultPair = remainingVC.sortWith((x, y) => (x._2).compareTo(y._2) < 0)
    // Returning String
    val resultWord = resultPair.map(_._1).mkString
    resultWord

  }
}


