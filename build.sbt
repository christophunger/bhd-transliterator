//import Dependencies._

ThisBuild / scalaVersion     := "2.13.6"
ThisBuild / version          := "0.8"

//java -Dfile.encoding=UTF8
//javacOptions ++= Seq("-encoding", "UTF8")
//javacOptions ++= Seq("-Dfile.encoding=UTF8")

lazy val root = (project in file("."))
  .settings(
    name := "bhd-transliterator",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.0.8" % Test,
      "com.github.tototoshi" %% "scala-csv" % "1.3.6"
    ),
    scalacOptions += "-deprecation"

  )
