# Behdînî-Kurdish Arabic script to Latin script converter

This is **work in progress**. 

## Purpose

This program converts Behdînî-Kurdish texts written in Arabic script into Latin script. 
It attempts to solve the following problems: 

	- The disambiguation between the vowels 'î' and 'û' and their 
	  corresponding semivowels 'y' and 'w'. 
	- The insertion of the neutral vowel 'i' after and between certain consonant 
      sequences, according to syllable structure rules. 
	  
While solving the practical task of transliteration from Arabic script to Latin script 
is the most tangible goal of this program, 
this software was developped for other reasons as well: 

    - To test theoretically grounded phonological analyses of Behdînî-Kurdish 
	  metrical phonological phenomena.
    - To develop heuristic procedures for the recognition of syllable structure 
	  and metrical phonology in this language. 
    - For the main author to practice programming principles in Scala (https://scala-lang.org/). 
	
For these reasons the internals of this program may change significantly 
in the future 
as new linguistic insights into Kurdish phonology are gained, 
or new programming techniques become available. 
	  
## Design principles

To properly solve the stated problems according to the phonology of Kurdish, 
one would have to implement a phonological parser with recourse to a dictionary. 
This program avoids these complications by using heuristic procedures 
that approximate the phonological rules. 

As a result of the use of heuristic procedures, 
the output of this program will never be perfect. 
It will always have to be manually cleaned. 

## Prerequisites and installation instructions

This program is written in Scala (https://scala-lang.org/) 
and compiles to Java Virtual Machine (JVM) bytecode. 
To run or compile this program, you need to have Java 11 LTS installed on your machine. 
More recent Java versions may also work but are untested. 

You can install this software by cloning this repository with Git (recommended), 
or downloading the repository as a .zip file 
and building from source.
For this process, 
you need the Simple Build Tool for Scala (sbt) 
installed on your system, see https://www.scala-sbt.org/

After clone the repository of bhd-transliterator with Git 
or downloading and uncompressing its .zip file, 
cd into your local repository directory
and run sbt from the commandline. 
This starts the sbt console. 
The first time sbt is run in this directory, 
it will download all necessary Scala files and dependcies 
for compiling and running the program. 

Once the sbt console is done with the initial setup, 
you can then run the program 
from the sbt console by typing the following command: 

```
run infile outfile [on] [path-to-wordlist]
```

See the section on 'Usage' for more information on the available 
commandline arguments. 

To build an executable .jar file, 
use the following command from the commandline 
(not the sbt console): 

```
sbt assembly
```

The assembled "fat" .jar file is located in the "target" subfolder 
of your local working directory, 
and named "bhd-transliterator-assembly-X.X.jar": 

```
bhd-transliterator/target/scala-2.13/bhd-transliterator-assembly-0.8.jar
```

## Usage 

This is a commandline program taking two arguments: 
file names for the input and the output file. 
Both of these files are text only files in UTF-8 encoding. 

Starting with version 0.3, this program accepts input files 
in Arabic script that may have Standard Format Markers. 
It ignores these SFM markers and works only on words and punctuation. 
To run this program from the commandline: 

```
java -jar bhd-transliterator-assembly-0.4.jar infile outfile
```
Starting with version 0.6, 
there is an option to switch on a function 
that modifies the output by removing the neutral vowel 'i' 
in the second syllable of words with at least four syllables (before 'i' removal). 
The reason for this is that 
the neutral vowel is often (but not always) not licensed by metrical structure 
in this position. 
If you want to turn this option on, 
add a third commandline argument "on": 

```
java -jar bhd-transliterator-assembly-0.6.jar infile outfile on
```
If the third commandline argument is omitted, 
or if the third commandline argument is anything else but the string "on", 
this function is turned off. 

Version 0.7 adds an optimisation: 
it applies the function removing 'i' in the second syllable of words with more than 4 syllables 
only in case the word is not one among a list of words with 'i' in its stem. 
This list is hardcoded for now and is known to be incomplete. 
Future versions will make this list customizable by the user. 
Use version 0.7 with the same arguments as version 0.6: 

```
java -jar bhd-transliterator-assembly-0.7.jar infile outfile on
```
In version 0.8, this wordlist with words whose stem contains 'i' is read from a file 
that must be provided on the commandline after "on": 

```
java -jar bhd-transliterator-assembly-0.8.jar infile outfile on wordlist
```

A wordlist of some 600+ words with /i/ in the stem is provided in the resources folder 
of the source tree: 

```
src/main/resources/stems_with_i.txt
```

Feel free to adjust this wordlist to your needs or use a different one. 

Version 0.8 can also be run without the wordlist-based optimization: 

```
java -jar bhd-transliterator-assembly-0.8.jar infile outfile off
```
or: 

```
java -jar bhd-transliterator-assembly-0.8.jar infile outfile
```

## Related software

A related software project is the Kurdish Language Library (KLL) by Dolan Hêrîş at Github: 
https://github.com/dolanskurd/kurdish

The Kurdish Language Library is a library written in Python 
providing convenience functions for dealing with Kurdish NLP, 
including script conversions in both directions between 
Arabic script and Latin script, for Soranî as well as Behdînî. 
This library uses a string-based approach to the problem of 
/i/ - insertion and semivowel disambiguation. 
KLL also provides a GUI and a web interface. 

The Bhd-transliterator, on the other hand, has a narrower focus: 
it provides Arabic script to Latin script conversion only, 
and is only designed for the Behdînî dialect of Kurdish. 
It does not provide other convenience functions for NLP. 
Its approach to /i/ - insertion and semivowel disambiguation 
is based on heuristic processes modelled after a linguistic 
analysis of the metrical phonology of Kurdish, 
and does not use a string-based approach. 
Theoretically, this approach should lead to better results, 
but the heuristic nature of the procedures implemented 
puts limits on the results achieved in practice. 
So your mileage may vary, 
and I encourage you to make your own comparisons. 


## License notice

Bhd-transliterator - transliterating Behdînî-Kurdish texts from Arabic script to Latin script. 
Copyright (C) 2021  Christoph Unger

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
