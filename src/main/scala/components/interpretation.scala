/* 
 *  This file is part of Bhd-transliterator.
 *  Copyright (C) 2021  Christoph Unger
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package interpretation

import phonemes.Phonemes._
import scala.util.matching.Regex

object Interpretation {

  def algorithm(c: Char): String = c match {
    case a if (consonantsAll(c)) => inserti(a.toString)
    case a if (allVowels(c)) => disambv(a.toString)
    case a => a.toString
  }

  // Semivowel disambiguation
  def disambv(in: String): String = {
    // See def inserti for explanation of these steps
    val following = in.drop(1) :+ '&'
    val nextfollowing = following.drop(1) :+ '&'
    val previous = '#' +: in.init
    val nextprevious = '#' +: previous.init
    val quintupled = nextprevious zip previous zip in zip following zip nextfollowing map {
      case ((((a, b), c), d), e) => (a,b,c,d,e)
    }

    // Semivowel disambiguation npptnf = nex-previous previous this following next-following
    // Position of character being worked on: c
    def vowelDisamb(npptfnf: (Char, Char, Char, Char, Char)): (Char, Char, Char, Char, Char, Char) = npptfnf match {
      case (a,b,c,d,e) if (isBeginningOfWord(b) && (c == 'u') && isEndOfWord(d)) => (a,b,'û',d,e,'0') // conjunction 'û'
      case (a,b,c,d,e) if ((consonantsAll(b) && !(b == 'x')) && (c == 'u') && (d == 'î')) => (a,b,c,d,e,'0') // huîn , hün
      case (a,b,c,d,e) if ((isBeginningOfWord(b) || liquids(b) || fricatives(b)) && (c == 'u') && allVowels(d)) => (a,b,'w',d,e,'0') // wî, wê
      case (a,b,c,d,e) if ((b == 'u') && (c == 'î')) => (a,b,c,d,e,'0') // exception to the next rule for 'wî' not 'wy'
      case (a,b,c,d,e) if (allVowels(b) && ((c == 'u') || (c == 'û')) && allVowels(d)) => (a,b,'w',d,e,'0') // peywendiyeka
      case (a,b,c,d,e) if ((midVowels(b) || lowVowels(b) || (b == 'û')) && (c == 'î')) => (a,b,'y',d,e,'0')
      case (a,b,c,d,e) if ((midVowels(b) || lowVowels(b)) && ((c == 'u') || (c == 'û'))) => (a,b,'w',d,e,'0')
      case (a,b,c,d,e) if (isBeginningOfWord(b) && (c == 'î') && allVowels(d)) => (a,b,'y',d,e,'0') // ya
      case (a,b,c,d,e) if ((c == 'î') && allVowels(d)) => (a,b,c,d,e,'y')
      case (a,b,c,d,e) if (consonantsAll(b) && (c == 'u') && (d == 'î')) => (a,b,c,d,e,'0') // duîv
      case (a,b,c,d,e) if (allVowels(b) && ((c == 'û') || (c == 'u')) && allVowels(d)) => (a,b,c,d,e,'w')
      case (a,b,c,d,e) => (a,b,c,d,e,'0')
    }

    def unpackSextupleV(sext: (Char, Char, Char,Char,Char,Char)): String = sext match {
      case (a,b,c,d,e,f) if (f == 'y') => c.toString concat f.toString
      case (a,b,c,d,e,f) if (f == 'w') => c.toString concat f.toString
      case (a,b,c,d,e,f) if (f == '0') => c.toString 
      case (_,_,c,_,_,_) => c.toString
    }

    // unpacking the sequence of sextuples into the result string
    val vowelDis = quintupled.map(vowelDisamb(_))
    vowelDis.map(unpackSextupleV(_)).mkString
  }

  
  // /i/ insertion
  def inserti(in: String): String = {
    // In order to find the right place in the syllable to insert a missing /i/,
    // we need to move from phoneme to phoneme and compare what the following phoneme is,
    // and sometimes also what the previous one was.
    // To do this, we turn the word from a sequence of characters
    // into a sequence of triples of characters: the first element is the previous phoneme,
    // the second one is the present phoneme, and the third one represents the following phoneme.
    // Actually, we also sometimes need to check the 2nd preceding and 2nd following character as well.
    // The first step in creating this sequence of pairs is to drop the first character
    // from each input word, and adding '&' at the end to mark the end of word,
    // and do the same for the one following thereafter: 
    val following = in.drop(1) :+ '&'
    val nextfollowing = following.drop(1) :+ '&'
    // then we add '#' as begin of word and drop the last character,
    // same for the next previous one:
    val previous = '#' +: in.init
    val nextprevious = '#' +: previous.init
    // then we do a fivefold zip
    val quintupled = nextprevious zip previous zip in zip following zip nextfollowing map {
      case ((((a, b), c), d), e) => (a,b,c,d,e)
    }

 
    // This function checks for conditions for inserting /i/.
    // If it finds them, it inserts the character 'i' as sixth element in a sextuple
    // Position of character worked on: c
    def findNv(quintuple: (Char, Char, Char, Char, Char)): (Char, Char, Char, Char, Char, Char) = quintuple match {
      case (a,b,c,d,e) if (b == '\\') => (a,b,c,d,e,'0') // don't operate on inline SFM
      case (a,b,c,d,e) if (c == '\\') => (a,b,c,d,e,'0') // don't operate on inline SFM
      case (a,b,c,d,e) if ((c == 's') && (d == 't')) => (a,b,c,d,e,'0') // 'st' is special
      case (a,b,c,d,e) if (isBeginningOfWord(b) && consonantsAll(c) && isEndOfWord(d)) => (a,b,c,d,e,'i') // one letter prepositions
      case (a,b,c,d,e) if (isBeginningOfWord(b) && consonantsAll(c) && consonantsAll(d)) => (a,b,c,d,e,'i') // two letter prepositions 'nik'
      case (a,b,c,d,e) if ((c == 'l') && (d == 'l')) => (a,b,c,d,e,'0') // 'millet'
      case (a,b,c,d,e) if ((c == 'k') && (d == 'r')) => (a,b,c,d,e,'i') // special case 'kirin'
      case (a,b,c,d,e) if (plosivesVoiced(c) && aspirant(d)) => (a,b,c,d,e,'i') // nabihîsin
      case (a,b,c,d,e) if (allVowels(b) && plosives(c) && plosives(d) && allVowels(e)) => (a,b,c,d,e,'0') // exception to following rule
      case (a,b,c,d,e) if (plosives(c) && plosives(d)) => (a,b,c,d,e,'i') // dkrn - dikirin
      case (a,b,c,d,e) if (plosives(c) && (fricatives(d) || aspirant(d))) => (a,b,c,d,e,'0')
      case (a,b,c,d,e) if (plosives(c) && liquidNasal(d)) => (a,b,c,d,e,'i')
      case (a,b,c,d,e) if ((fricatives(c) || aspirant(c)) && plosives(d)) => (a,b,c,d,e,'0')
      case (a,b,c,d,e) if ((fricatives(c) || aspirant(c)) && (fricatives(d) || aspirant(d)) && consonantsAll(e)) => (a,b,c,d,e,'i')
      case (a,b,c,d,e) if ((allVowels(b) || liquids(b)) &&  aspirant(c) && liquidNasal(d)) => (a,b,c,d,e,'i') // berhingarî
      case (a,b,c,d,e) if (((b == 'b') || (b == 'd')) && (c == 'ç') && nasals(d)) => (a,b,c,d,e,'i') // for verb forms 'çûn'
      case (a,b,c,d,e) if (((b == 'b') || (b == 'd')) && (c == 'n') && (d == 'v')) => (a,b,c,d,e,'i') // for verb forms 'nivîsin'
      case (a,b,c,d,e) if ((fricatives(b) || liquids(b) || aspirant(b)) && fricatives(c) && liquidNasal(d)) => (a,b,c,d,e,'i') // 'hizir' not 'hizr'
      case (a,b,c,d,e) if (fricatives(c) && liquidNasal(d)) => (a,b,c,d,e,'i') // difinbilind
      case (a,b,c,d,e) if (liquidNasal(c) && plosives(d)) => (a,b,c,d,e,'0')
      case (a,b,c,d,e) if ((liquidNasal(c) || aspirant(c) ) && fricatives(d)) => (a,b,c,d,e,'0')
      case (a,b,c,d,e) if (liquidNasal(c) && liquidNasal(d)) => (a,b,c,d,e,'i')
      case (a,b,c,d,e) => (a,b,c,d,e,'0')
    }

    // applying /i/ insertion to a word in the input
    val iInserted = quintupled.map(findNv(_))


    def unpackSextupleC(sext: (Char, Char, Char,Char,Char,Char)): String = sext match {
      case (a,b,c,d,e,f) if (f == 'i') => c.toString concat f.toString
      case (a,b,c,d,e,f) if (f != 'i') => c.toString 
      case (_,_,c,_,_,_) => c.toString
    }

    // unpacking the sequence of sextuples into the result string
    iInserted.map(unpackSextupleC(_)).mkString
  }

}
