/* 
 *  This file is part of Bhd-transliterator.
 *  Copyright (C) 2021  Christoph Unger
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package phonemes

import scala.util.matching.Regex

object Phonemes {
  // Consonants
  // Plosives
  val plosivesVoiced = Set('b','d','g')
  val plosivesVoiceless = Set('p','t','k','q','\u0294')
  val plosives = plosivesVoiceless ++ plosivesVoiced

  // Fricatives
  val fricativesVoiceless = Set('f','s','ş','x','\u0127', 'ç') // with affricates
  val fricativesVoiced = Set('v','z','j','\u0295', 'c') // with affricates
  val fricatives = fricativesVoiceless ++ fricativesVoiced

  // Nasals, Liquids, Approximants, Semivowels
  val nasals = Set('m','n')
  val liquids = Set('l','ĺ','r','ŕ') // 'h' approximant?
  val liquidNasal = liquids ++ nasals
  val semivowels = Set('y','w') // 'h' semivowel?
  val aspirant = Set('h') // 'h' does not fit in nicely with other categories

  // more consonant subtypes, if needed
  val consonantsVoiceless = plosivesVoiceless ++ fricativesVoiceless
  val consonantsAll = plosives ++ fricatives ++ nasals ++ liquids ++ aspirant // ++ semivowels
  val consonantsNoSemi = plosives ++ fricatives ++ nasals ++ liquids
  val consonantsVoiced = consonantsAll filter (x => !(consonantsVoiceless contains x))
  val consonantsNoPlosives = fricatives ++ nasals ++ liquids // ++ semivowels
  val consonantsApproximants = nasals ++ liquids // ++ semivowels

  // Vowels
  val highVowels = Set('î','ü','i','û','u')
  val midVowels = Set('ê','e','o')
  val lowVowels = Set('a')

  val frontVowels = Set('î','ü','ê')
  val backVowels = Set('û','u','o')
  val centralVowels = Set('i','e','a')

  val allVowels = highVowels ++ midVowels ++ lowVowels

  val shortVowels = Set('i','u','e')
  val longVowels = allVowels filter (x => !(shortVowels contains x))
  val neutralVowel = Set('i')

  val licensingVowels = allVowels excl 'i'

  val roundedVowels = Set('ü','û','u','o')
  val unroundedVowels = allVowels filter (x => !(roundedVowels contains x))
  val unroundedFrontVowels = frontVowels filter (x => !(roundedVowels contains x))

  val vowelsToDisambiguate = Set('î','û')

  // punctuation
  val punct = Set('.',',','?','!',';',':','"','«','»','‹','›')

  // End of Word mark '&'
  def isEndOfWord(c: Char): Boolean = {
    if ((c == '&') || punct(c)) true
    else false
  }

  // Beginning of Word mark '#'
  def isBeginningOfWord(c: Char): Boolean = {
    if ((c == '#') || punct(c)) true
    else false
  }

  def isHighFrontV(c: Char): Boolean = {
    if (c == 'î') true
    else false
  }

  def isHighBackV(c: Char): Boolean = {
    if (c == 'û') true
    else false
  }


}
