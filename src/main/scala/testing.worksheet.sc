import scala.util.matching.Regex

object Phonemes {
  // Consonants
  // Plosives
  val plosivesVoiced = Set('b','d','g')
  val plosivesVoiceless = Set('p','t','k','q','\u0294')
  val plosives = plosivesVoiceless ++ plosivesVoiced

  // Fricatives
  val fricativesVoiceless = Set('f','s','ş','x','\u0127', 'ç') // with affricates
  val fricativesVoiced = Set('v','z','j','\u0295', 'c') // with affricates
  val fricatives = fricativesVoiceless ++ fricativesVoiced

  // Nasals, Liquids, Approximants, Semivowels
  val nasals = Set('m','n')
  val liquids = Set('l','ĺ','r','ŕ') // 'h' approximant?
  val liquidNasal = liquids ++ nasals
  val semivowels = Set('y','w') // 'h' semivowel?
  val aspirant = Set('h') // 'h' does not fit in nicely with other categories

  // more consonant subtypes, if needed
  val consonantsVoiceless = plosivesVoiceless ++ fricativesVoiceless
  val consonantsAll = plosives ++ fricatives ++ nasals ++ liquids ++ aspirant // ++ semivowels
  val consonantsNoSemi = plosives ++ fricatives ++ nasals ++ liquids
  val consonantsVoiced = consonantsAll filter (x => !(consonantsVoiceless contains x))
  val consonantsNoPlosives = fricatives ++ nasals ++ liquids // ++ semivowels
  val consonantsApproximants = nasals ++ liquids // ++ semivowels

  // Vowels
  val highVowels = Set('î','ü','i','û','u')
  val midVowels = Set('ê','e','o')
  val lowVowels = Set('a')

  val frontVowels = Set('î','ü','ê')
  val backVowels = Set('û','u','o')
  val centralVowels = Set('i','e','a')

  val allVowels = highVowels ++ midVowels ++ lowVowels

  val shortVowels = Set('i','u','e')
  val longVowels = allVowels filter (x => !(shortVowels contains x))
  val neutralVowel = Set('i')

  val licensingVowels = allVowels.-('i')

  val roundedVowels = Set('ü','û','u','o')
  val unroundedVowels = allVowels filter (x => !(roundedVowels contains x))
  val unroundedFrontVowels = frontVowels filter (x => !(roundedVowels contains x))

  val vowelsToDisambiguate = Set('î','û')

  // punctuation
  val punct = Set('.',',','?','!',';',':','"','«','»','‹','›')

  // End of Word mark '&'
  def isEndOfWord(c: Char): Boolean = {
    if ((c == '&') || punct(c)) true
    else false
  }

  // Beginning of Word mark '#'
  def isBeginningOfWord(c: Char): Boolean = {
    if ((c == '#') || punct(c)) true
    else false
  }

  def isHighFrontV(c: Char): Boolean = {
    if (c == 'î') true
    else false
  }

  def isHighBackV(c: Char): Boolean = {
    if (c == 'û') true
    else false
  }

  // traits and classes for phonemes and syllables
  // At first sight it may look like needless duplication to set up case classes
  // on the basis of the classification of phonemes into sets,
  // but it may greatly simplify pattern matching for the interpretation. 
  trait Phoneme

  // Main categories: consonants and vowels
  trait Consonant
  trait Vowel
  
  // subtypes of consonants
  trait Sonorant
  trait Nonsonorant
  trait Voiced
  trait Voiceless

  case class Plosive(c: Char) extends Consonant with Phoneme with Nonsonorant { require(plosives(c)) }
  case class PlosiveVoiced(c: Char) extends Consonant with Phoneme with Nonsonorant with Voiced { require(plosivesVoiced(c)) }
  case class PlosiveVoiceless(c: Char) extends Consonant with Phoneme with Nonsonorant with Voiceless { require(plosivesVoiceless(c)) }

  case class Fricative(c: Char) extends Consonant with Phoneme { require(fricatives(c)) }
  case class FricativeVoiced(c: Char) extends Consonant with Phoneme with Voiced { require(fricativesVoiced(c)) }
  case class FricativeVoiceless(c: Char) extends Consonant with Phoneme with Voiceless { require(fricativesVoiceless(c)) }

  case class Nasal(c: Char) extends Consonant with Phoneme with Sonorant with Voiced { require(nasals(c)) }
  case class Liquid(c: Char) extends Consonant with Phoneme with Sonorant with Voiced { require(liquids(c)) }
  case class Aspirant(c: Char) extends Consonant with Phoneme with Voiceless { require(aspirant(c)) }

  case class Semivowel(sv: Char) extends Consonant with Phoneme { require(semivowels(sv)) }

  // subtypes of vowels
  case class Highvowel(v: Char) extends Vowel with Phoneme { require(highVowels(v)) }
  case class Midvowel(v: Char) extends Vowel with Phoneme { require(midVowels(v)) }
  case class Lowvowel(v: Char) extends Vowel with Phoneme { require(lowVowels(v)) }

  case class Frontvowel(v: Char) extends Vowel with Phoneme { require(frontVowels(v)) }
  case class Centralvowel(v: Char) extends Vowel with Phoneme { require(centralVowels(v)) }
  case class Backvowel(v: Char) extends Vowel with Phoneme { require(backVowels(v)) }

  case class Shortvowel(v: Char) extends Vowel with Phoneme { require(shortVowels(v)) }
  case class Longvowel(v: Char) extends Vowel with Phoneme { require(longVowels(v)) }
  case class Neutralvowel(v: Char) extends Vowel with Phoneme { require(neutralVowel(v)) }
  case class Licensingvowel(v: Char) extends Vowel with Phoneme { require(licensingVowels(v)) }

  case class Roundedvowel(v: Char) extends Vowel with Phoneme { require(roundedVowels(v)) }
  case class Unroundedvowel(v: Char) extends Vowel with Phoneme { require(unroundedVowels(v)) }
  case class Unroundedfrontvowel(v: Char) extends Vowel with Phoneme { require(unroundedFrontVowels(v)) }

  case class Voweltodisambiguate(v: Char) extends Vowel with Phoneme { require(vowelsToDisambiguate(v)) }

  trait Syllable
  trait Open
  trait Closed
  case class Opensyllable(onset: Consonant, nucleus: Vowel) extends Syllable with Open
  case class Closedsyllable(onset: Consonant, nucleus: Vowel, coda: Consonant) extends Syllable with Closed
  case class Doubleclosedsyllable(onset: Consonant, nucleus: Vowel, coda1: Sonorant, coda2: Consonant) extends Syllable with Closed
  // case class Specialonsetsyllableclosed(on1: Consonant, on2: Consonant, on3: Consonant, nucleus: Vowel, coda: Consonant) extends Syllable with Closed {
  //   require(on1 == 's' && on2 == 't' && on3 == 'r')
  // }
  // case class Specialonsetsyllableopen(on1: Consonant, on2: Consonant, on3: Consonant, nucleus: Vowel) extends Syllable with Open {
  //   require(on1 == 's' && on2 == 't' && on3 == 'r')
  // }
  // case class Conjunctionsyllable(nucleus: Vowel) extends Syllable with Open {
  //   require(nucleus == 'û')
  // }

  trait Word
  case class Monosyllabicword(s: Syllable) extends Word
  case class Disyllabicword(s1: Syllable, s2: Syllable) extends Word
  case class Trisyllabicword(s1: Syllable, s2: Syllable, s3: Syllable) extends Word
  case class Foursyllableword(s1: Syllable, s2: Syllable, s3: Syllable, s4: Syllable) extends Word
  case class Fivesyllableword(s1: Syllable, s2: Syllable, s3: Syllable, s4: Syllable, s5: Syllable) extends Word
  case class Sixsyllableword(s1: Syllable, s2: Syllable, s3: Syllable, s4: Syllable, s5: Syllable, s6: Syllable) extends Word

}

val wordWrong = "destihelata"
// Making a List of Char - Index number pairs from wordWrong
val wordWrongIndexed = wordWrong.toList.zipWithIndex
// Making a List of Triples only of the Vowels
val wordWrongVowels = wordWrongIndexed
  .filter{case (v, i) if Phonemes.allVowels(v) => true; case _ => false}
  .zipWithIndex
  .map{case ((a, b), c) => (a, b, c)} 

def betterAssignLicenseScore(voweltuples: List[(Char, Int, Int)]): List[(Char, Int, Int, Int)] = {
  val maxind = voweltuples.length
  def assignValue(tu: (Char, Int, Int)): (Char, Int, Int, Int) = tu match {
    case (ch, cix, vix) if ch == 'i' && vix > 0 && vix < maxind - 1 => (ch, cix, vix, 0)
    case (ch, cix, vix) => (ch, cix, vix, 1)
  }
  
  for (t <- voweltuples) yield assignValue(t)
}

val wordWrongVowelsLicense = betterAssignLicenseScore(wordWrongVowels)

def getIndexToFilter(chartuple: (Char, Int, Int, Int)): Boolean = chartuple match {
  case (_, ix, _, li) if li == 0 => true
  case _ => false
}

//val filtered = wordWrongIndexed.filterNot{ case (c, i) if i == 4 => true ; case _ => false }
val filtered = wordWrongVowelsLicense.filterNot(getIndexToFilter(_))

val remainigVowelPairs = filtered.map(x => (x._1, x._2))

val consonantPairs = wordWrongIndexed.filter{ case (c, i) if Phonemes.consonantsAll(c) => true ; case _ => false }

val remainingVC = remainigVowelPairs ::: consonantPairs

val resultPair = remainingVC.sortWith((x, y) => (x._2).compareTo(y._2) < 0)
val resultWord = resultPair.map(_._1).mkString

val (vowelsOfWordWrong, consonantsOfWordWrong) = wordWrongIndexed
  .partition{
    case (c, i) if Phonemes.allVowels(c) => true
    case _ => false }

vowelsOfWordWrong
consonantsOfWordWrong

vowelsOfWordWrong
  .zipWithIndex
  .map{ case ((a, b), c) => (a, b, c)}



